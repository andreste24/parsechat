//
//  Payments.swift
//  SwiftParseChat
//
//  Created by Andres Trevino on 1/7/16.
//  Copyright © 2016 Jesse Hu. All rights reserved.
//

import UIKit
import Alamofire

class Payments: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  @IBAction func tokenizeCard(sender: UIButton) {
    let conekta = Conekta()
    conekta.delegate = self
    conekta.publicKey = "key_Jr4wEtqxPFxaqkSNGfpfhsA"
    conekta.collectDevice()
    let card = conekta.Card()
    card.setNumber("4242424242424242", name: "Julian Ceballos", cvc: "123", expMonth: "10", expYear: "2018")
    let token = conekta.Token()
    token.card = card
    token.createWithSuccess({ (data) -> Void in
      print("success")
      let tokenizedCard = data["id"] as! String
      
      Alamofire.request(.POST, "http://mexiley2-staging.herokuapp.com/api/v1/payment/make_charge", parameters: ["X-token" : "fcea920f7412b5da7be0cf42b8c93759", "conektaTokenId" : tokenizedCard])
        .response() {
        (request, response, data, error) in
        
        print("esta es la response: \(response)")
        print("esta es la request: \(request?.description)")
        print("esta es la error: \(error)")
      }
      
      }, andError: { (error) -> Void in
        print("este es el error \(error)")
    })
  }

}
